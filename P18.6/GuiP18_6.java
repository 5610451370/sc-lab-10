

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class GuiP18_6{
	
	private JFrame frame;
	private JMenuItem buttongreen;
	private JMenuItem buttonred;
	private JMenuItem buttonblue;
	private JPanel panel;
	private JPanel panelbg;
	private JPanel panelbutton;
	
	private JMenu menu;
	private JMenuBar menubar;

	
	public GuiP18_6(){
		frame = new JFrame("Test GUI");
		frame.setSize(400, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		createPanel();

	}
	
	public void createButton(){
		menu = new JMenu("Click");
		buttongreen = new JMenuItem("GREEN");
		buttonred = new JMenuItem("RED");
		buttonblue = new JMenuItem("BLUE");
		
		menu.add(buttongreen);
		menu.add(buttonred);
		menu.add(buttonblue);
		
		menubar = new JMenuBar();
		menubar.add(menu);
		menubar.setBackground(Color.YELLOW);
		buttongreen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panelbg.setBackground(Color.GREEN);
				
			}
		});
		
		buttonred.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panelbg.setBackground(Color.RED);
				
			}
			
		});
		
		buttonblue.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panelbg.setBackground(Color.BLUE);
				
			}
			
		});
	}

	
	public void createPanel(){
		panelbutton = new JPanel();
		panelbutton.setBackground(Color.black);
		
		panelbg = new JPanel();
		panelbg.setLayout(new BorderLayout());
		
		createButton();
		
		
		panelbg.add(menubar , BorderLayout.SOUTH);
		frame.add(panelbg);
	}
}
