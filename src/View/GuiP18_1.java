package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GuiP18_1{
	
	private JFrame frame;
	private JButton buttongreen;
	private JButton buttonred;
	private JButton buttonblue;
	private JPanel panel;
	private JPanel panelbg;
	private JPanel panelbutton;
	
	
	public GuiP18_1(){
		frame = new JFrame("Test GUI");
		frame.setSize(400, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		createPanel();

	}
	
	public void createButton(){
		buttongreen = new JButton("GREEN");
		buttonred = new JButton("RED");
		buttonblue = new JButton("BLUE");
		
		buttongreen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panelbg.setBackground(Color.GREEN);
				
			}
		});
		
		buttonred.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panelbg.setBackground(Color.RED);
				
			}
			
		});
		
		buttonblue.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panelbg.setBackground(Color.BLUE);
				
			}
			
		});
	}

	
	public void createPanel(){
		panelbutton = new JPanel();
		panelbutton.setBackground(Color.black);
		
		panelbg = new JPanel();
		panelbg.setLayout(new BorderLayout());
		
		createButton();
		panelbutton.add(buttongreen);
		panelbutton.add(buttonred);
		panelbutton.add(buttonblue);
		
		panelbg.add(panelbutton, BorderLayout.SOUTH);
		frame.add(panelbg);
	}
}
