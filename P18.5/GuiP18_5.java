

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GuiP18_5{
	
	private JFrame frame;
	private JPanel panel;
	private JPanel panelbg;
	private JPanel panelbutton;
	private JButton buttonok;
	String[] a = {"GREEN","RED","BLUE"};
	private JComboBox combo = new JComboBox(a);
	
	
	public GuiP18_5(){
		frame = new JFrame("Test GUI");
		frame.setSize(400, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		createPanel();

	}
	
	public void createButton(){
		buttonok = new JButton("OK");
		buttonok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (combo.getSelectedItem() == "GREEN"){
					panelbg.setBackground(Color.GREEN);
				}
				if (combo.getSelectedItem() == "RED"){
					panelbg.setBackground(Color.RED);
				}
				if (combo.getSelectedItem() == "BLUE"){
					panelbg.setBackground(Color.BLUE);
				}
				
			}
		});
		
	}

	
	public void createPanel(){
		panelbutton = new JPanel();
		panelbutton.setBackground(Color.black);
		
		panelbg = new JPanel();
		panelbg.setLayout(new BorderLayout());

		createButton();
		panelbutton.add(combo);
		panelbutton.add(buttonok);
		
		panelbg.add(panelbutton, BorderLayout.SOUTH);
		frame.add(panelbg);
	}
}
